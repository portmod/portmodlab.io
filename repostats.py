#!/usr/bin/env python3

from bs4 import BeautifulSoup
import requests
from typing import Optional

output = BeautifulSoup(
    """
        <table>
        <thead>
       <tr>
        <th>
            Game Engine
        </th>
        <th>
            Repositories / Packages
        </th>
       </tr>
      </thead>
      <tbody>
      </tbody>
        </table>
    """
)

# Pull package counts from repository sites and create table sorted by repository
total_counts = {}
for game, sites in {
    "OpenMW": ["openmw-mods"],
    "Kerbal Space Program": ["ksp", "ckan"],
    "Kerbal Space Program 2": ["ksp2"],
    "GZDoom": ["zdoom"],
    "X3: Albion Prelude": ["x3"],
    "Fallout: New Vegas": ["fallout-nv"],
#    "Fallout 4": ["fallout-4"],
    "The Elder Scrolls IV: Oblivion": ["oblivion"],
}.items():

    site_packages = []
    total_count = 0
    for site in sites:
        url = f"https://portmod.gitlab.io/{site}"
        rindex = requests.get(url)
        if rindex.status_code != requests.codes.ok:
            rindex.raise_for_status()

        soup = BeautifulSoup(rindex.text)
        count = int(soup.find_all("td")[1].string.strip())
        site_packages.append((site, count))
        total_count += count
    total_counts[total_count] = (game, site_packages)


def create_link(root: BeautifulSoup, parent: BeautifulSoup, href: str, name: str):
    link = root.new_tag("a", href=href)
    link.string = name
    parent.append(link)
    return link


def create_col(root: BeautifulSoup, parent: BeautifulSoup, contents: str, width: Optional[str] = None):
    col = root.new_tag("td", width=width)
    col.string = contents
    parent.append(col)
    return col


for count, (game, sites) in reversed(sorted(total_counts.items(), key=lambda x: x[0])):
    row = BeautifulSoup("<tr></tr>")
    create_col(row, row, game)
    sites_table = BeautifulSoup("<table><tbody></tbody></table>")
    for site, count in reversed(sorted(sites, key=lambda x: x[1])):
        site_row = sites_table.new_tag("tr")
        site_col = create_col(sites_table, site_row, "")
        create_link(sites_table, site_col, href=f"https://portmod.gitlab.io/{site}", name=site)
        create_col(sites_table, site_row, str(count), width="35%")
        sites_table.tbody.append(site_row)

    create_col(row, row, "").append(sites_table)
    output.tbody.append(row)

print(output.prettify())
